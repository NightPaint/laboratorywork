/*
 * Author 
 *		Zagidullin Dmitry
 */

/* !--��������� ����� TItem--! ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------*/

#ifndef TITEM_H_																											// ���� �� ������ ����� ����������� (TITEM_H_) �� ��� �� ��������� ������ ���������� � ������
#define TITEM_H_																											// ���������� �� TITEM_H_

/* ������������ ���������� */
#include <cstring> 
#include <string>

/* ������������ ����� ��� ������������ � �������� ������� */
#include "../../Libraries/BigDecimal/BigDecimal.h"

/* ������������ ����������� ���� */
using namespace std;

class TItem {

private:
	int quantity�ores;																										// ���������� ����
	BigDecimal clockFrequency, dataSize, time, precent;																		// �������� ������� (���), ������ �������������� ������ (���), ������������ ����� (���)
	bool flag;																												// �������� ����������

	void setTime();																											// ����� ������������� ����������� ���������� time �� ������ ����������

public:
	TItem ();																												// ����������
	TItem (int quantity�ores, BigDecimal clockFrequency, BigDecimal dataSize, BigDecimal time);								// ���������� �����������
	TItem (int quantity�ores, BigDecimal clockFrequency, BigDecimal dataSize, bool flag);									// ���������� �����������
	virtual ~TItem ();																										// ����������
	int getQuantity�ores();																									// ������ ���������� Quantity �ores
	BigDecimal getClockFrequency();																							// ������ ���������� Clock Frequency
	BigDecimal getDataSize();																								// ������ ���������� Data Size
	BigDecimal getTime();																									// ������ ���������� Time
	BigDecimal getPrecent();																								// ������ ���������� precent
	void setPrecent(double precents);
	void setTimet(int times);
};

#endif TITEM_H_																												// end