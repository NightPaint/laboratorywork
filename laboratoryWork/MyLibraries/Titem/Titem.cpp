/*
 * Author 
 *		Vagapova Lina
 */

/* !--�������--! -------------------------------------------------------------------------------------------------------------------------------------------------------------------*/

/* ������������ ���������� */
#include <cstring> 
#include <string>
#include <iostream>

/* ������������ ��������� */
#include "TItem.h"

/* ������ */
#define print(X) (std::cout << X << "\n")																																// ���������� ����� ������
#define null (std::cout << "\n")																																		// ���������� ������� ������

/* ������������ ����������� ���� */
using namespace std;

// ������ ���������� quantity�ores
int TItem::getQuantity�ores() {
	return quantity�ores;
}

// ������ ���������� precent
BigDecimal TItem::getPrecent() {
	return precent;
}

// ������ ���������� clockFrequency
BigDecimal TItem::getClockFrequency() {
	return clockFrequency;
}

// ������ ���������� dataSize
BigDecimal TItem::getDataSize() {
	return dataSize;
}

// ������ ���������� time
BigDecimal TItem::getTime() {
	return time;
}

/* ����� ������������� ����������� ���������� language �� ������ ���������� */
void TItem::setTime() {
	if (time == NULL) {
		if (quantity�ores >= 8 && clockFrequency >= 2.1 || dataSize < 15361) {
			time = 1;
			precent = 100;
		} else {
			time = -1;
			precent = -100;
		}
	} else
		print("������, � �������� ��� ���������� �����...");
}

/* ��������� ������ */
TItem::~TItem() {

}

/* ���������� ������ */
TItem::TItem () {
	quantity�ores = 0;
	clockFrequency = 0;
	dataSize = 0;
	time = 0;
}

/* ���������� ������������ */
TItem::TItem (int quantity�oress, BigDecimal clockFrequencys, BigDecimal dataSizes, BigDecimal times) {
		quantity�ores = quantity�oress;
		clockFrequency = clockFrequencys;
		dataSize = dataSizes;
		time = times;
}

/* ���������� ������������ */
TItem::TItem (int quantity�oress, BigDecimal clockFrequencys, BigDecimal dataSizes, bool flag) {
		quantity�ores = quantity�oress;
		clockFrequency = clockFrequencys;
		dataSize =  dataSizes;
		if (flag) {
			time = NULL;
			setTime();
		} else {
			time = 0;
			precent = 0;
		}
}

void TItem::setPrecent(double precents) {
	precent = precents;
}

void TItem::setTimet(int times) {
	time = times;
}