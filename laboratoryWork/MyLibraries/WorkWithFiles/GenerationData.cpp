/*
 * Author 
 *		Zagidullin Dmitry
 */

/* !--��������� ������--! ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------*/

/* ������������ ���������� */
#include <iostream>  
#include <list>
#include <ctime>
#include <windows.h> 

/* ������������ ������������ ����� */
#include "../Titem/Titem.h"

/* ������ */
#define print(X) (cout << X << "\n")																												// ���������� ����� ������
#define null (cout << "\n")																															// ���������� ������� ������

/* ���������� ��������� ������� */
extern void mainfunctionForCreateAndSaveXml(list<TItem> data);																						// �������� � ����� ��������������� ������ � xml ����

/* ������������ ����������� ���� */
using namespace std;

int MyGenQuantity�ores();																															// ��������� ���������� ����
double MyGenClockFrequency();																														// ��������� ������� ����������
void precentSeeGeneratedData();																														// ���������� �����																								

/* �������� ����� ������ */
/* @param col - ���������� ��������� */
list<TItem> generationData(int col) {

	srand(time(NULL));																																// ����� ��� ������� ������ ���� ������ �����
	list<TItem> listTItem;																															// ���� ��������� ���������

	/* ��������� ���������� */
	double tempGenQuantity�ores;
	double tempGenClockFrequency;
	double tempDataSize;

	while (col != 0) {																																// ���� ���������� �� ����� ����

		tempGenQuantity�ores = MyGenQuantity�ores();
		tempGenClockFrequency = MyGenClockFrequency();
		int tempDataSize = (1000000 + rand() % 17592186044416) * 1024;

		listTItem.push_back(TItem(tempGenQuantity�ores, tempGenClockFrequency, tempDataSize, true));												// ��������� � ���� ������� � ���������� ����������
	
		col--;																																		// ��������� ���������� ��������� �� 1
	}

	precentSeeGeneratedData();																													// ����� ���������

	mainfunctionForCreateAndSaveXml(listTItem);																										// �������� � ����� ��������������� ������ � xml ����

	return listTItem;																																// ���������� ��������� ����
}

double MyGenClockFrequency() {
	double arr[28] = {0.9, 1.0, 1.1, 1.2, 1.3, 1.4, 1.5, 1.6, 1.7, 1.8, 1.9, 2.0, 2.1, 2.2, 2.3, 2.4, 2.5, 2.6, 2.7, 2.8, 2.9, 3.0, 3.1, 3.2, 3.3, 3.4, 3.5, 3.6};

    return arr[rand() % 28];
}

int MyGenQuantity�ores() {
	int arr[4] = {2, 4, 8, 16};

	return arr[rand() % 4];
}

/* ����� ������ */
/* ����� ��������� */
void precentSeeGeneratedData() {
	print("�������� ���� ��������� � ��������� ��������� ������ ������... (10%)");
	Sleep(500); 
	print("�������� ���� ��������� � ��������� ��������� ������ ������... (75%)");
	Sleep(650); 
	print("�������� ���� ��������� � ��������� ��������� ������ ������... (100%)");
	Sleep(500); 
	print("�������� ���� ��������� � ��������� ��������� ������ ������... (completed)");

	int precent = 0;
	while (precent <= 100) {
		print(("���� ��������� ������, ���������� ��������� (" + to_string(precent) + "%)"));
		Sleep(50); 
		precent++;
	}

	print(("���� ��������� ������, ���������� ��������� (completed)"));
	Sleep(500); 
	print("��������� ������ �����������, ������� �� ��������...");

	null;
}