/*
 * Author 
 *		Vagapova Lina
 */

/* !--���������� �� XML--! -------------------------------------------------------------------------------------------------------------------------------------------------------------------*/

/* ������������ ���������� */
#include <iostream>  
#include <list>
#include <windows.h> 

/* ������������ ������������ ����� */
#include "../../Libraries/TINY/tinyxml2.h"
#include "../Titem/Titem.h"

/* ������ */
#define print(X) (std::cout << X << "\n")																														// ���������� ����� ������
#define null (cout << "\n")																																		// ���������� ������� ������

/* ������������ ����������� ���� */
using namespace tinyxml2;

void precentReadXML();																																			// ����� ���������

/* �������� ����� ������ */
/* return - ��������� ���� ��������� */
list<TItem> startReadXml() {

	tinyxml2::XMLDocument xmlDoc;																																// ������� xml ��������
	XMLError eResult;																																			// ������� ��������� ��������
	list<TItem> readTItem;																																		// ������� ���� ��������� ���������

	/* �������� ������ ���� � ������� ���������� */
	std::string path(getenv("HOMEPATH"));
	path += "\\Desktop\\LabWorkOne\\Data.xml";

	eResult = xmlDoc.LoadFile(path.c_str());																													// ��������� ����
	

	if (eResult != XML_SUCCESS)																																	// ���� ���� ������ � ������
	{
		print("������ ���� �� ������");																															// ������
		print("���������� �������� �� ���� ��������������...");																									// ������
	} else { 
		print("����� ����� ��������");																															// ����������
		print("������� ������� ����...");																														// ����������

		XMLNode *pRoot =  xmlDoc.FirstChild();																													// ��������� �������� ���

		if (pRoot == nullptr) {																																	// ���� �������� ��� �� ������ - �� ���� � ����� ������������ ������
			print("������ ���� ������");																														// ������
			print("���������� �������� �� ���� ��������������...");																								// ������
		} else {
				print("������ ������");																															// ����������
				print("���������� � ������ �����...");																											// ����������

				XMLElement *pElem = pRoot->FirstChildElement("Elem");																							// ��������� ��� 'Element'

				/* ���������� � ������� ����� ����������� ������ */
				int tempQuantity�ores;
				double tempClockFrequency, tempTime;
				int tempDataSize;

				while (pElem != nullptr) {																														// ���� ��������� ��� 'Elem' �� ������
		
					try {
						pElem->FirstChildElement("QuantityCores")->QueryIntText(&tempQuantity�ores);															// ��������� ������ �� ���� 'Quantity �ores'
						pElem->FirstChildElement("ClockFrequency")->QueryDoubleText(&tempClockFrequency);														// ��������� ������ �� ���� 'Clock Frequency'
						pElem->FirstChildElement("DataSize")->QueryIntText(&tempDataSize);																		// ��������� ������ �� ���� 'Data Size'
						pElem->FirstChildElement("Time")->QueryDoubleText(&tempTime);																			// ��������� ������ �� ���� 'Time'

						readTItem.push_back(TItem(tempQuantity�ores, tempClockFrequency, tempDataSize, tempTime));
					} catch (...) {
						print("������ ��� �������� ������������ �������� � ������ 'startReadXml', ���������� �������� �� ���� ��������������...");				// ������
					}
					/*--------------------------------------*/

					pElem = pElem->NextSiblingElement("Elem");																									// ��������� ��������� ��� 'Elem'
				}
		}
	}
			
	//precentReadXML();																																			// ����� ���������
	return readTItem;																																			// ����������� �����
}

/* ����� ������ */
/* ����� ��������� */
void precentReadXML() {
	print("�������� ���� ��������� � ������ ������... (28%)");
	Sleep(500); 
	print("�������� ���� ��������� � ������ ������... (37%)");
	Sleep(650); 
	print("�������� ���� ��������� � ������ ������... (95%)");
	Sleep(500); 
	print("�������� ���� ��������� � ������ ������... (100%)");
	Sleep(500);
	print("�������� ���� ��������� � ������ ������... (completed)");

	int precent = 0;
	while (precent <= 100) {
		print(("���� ������ ������ � �����, ���������� ��������� (" + to_string(precent) + "%)"));
		Sleep(50); 
		precent++;
	}
	print(("���� ������ ������, ���������� ��������� (completed)"));
	Sleep(500); 
	print("������ ������ �����������...");
	print("��� ���� ������, ���������� ���������... (37%)");
	Sleep(650); 
	print("��������� ���������, �������... (61%)");
	Sleep(650); 
	print("��� ��������� ���������, ������� �� ��������... (�completed)");
	null;
}