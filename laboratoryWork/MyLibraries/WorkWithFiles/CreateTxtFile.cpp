/*
 * Author 
 *		Zagidullin Dmitry
 */

/* !--�������� TXT �����--! ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------*/

/* ����������� ��������� */
#include <iostream>
#include <fstream>
#include <list>
#include <windows.h> 
#include <conio.h>
#include <time.h>

/* ����������� ������������ ������ */
#include "../Titem/Titem.h"

/* ������ */
#define print(X) (std::cout << X << "\n")																																// ���������� ����� ������
#define null (std::cout << "\n")																																		// ���������� ������� ������
								
/* ������������ ����������� ���� */
using namespace std;

void precentCreateTXt();

void startCreateTxt(list<TItem> listTItem) {

	print("��������� ���� ��� �������� ����... (completed)");

	/* �������� ������ ���� � �������� ����� */
	string path(getenv("HOMEPATH"));
	path += "\\Desktop\\LabWorkOne\\Data.txt";

	ofstream fileOut(path);
	fileOut << listTItem.size() << " 2 1 \n";

	if ( !listTItem.empty() ) {																																				// ���� ���� �� ������
		for (auto elem = listTItem.begin(); elem != listTItem.end(); elem++) {																								// ���������� ���� ����

			fileOut << elem->getQuantity�ores() << " " << elem->getClockFrequency().toString() << " \n";																	// << " " << elem->getDataSize().toString()
			fileOut << elem->getTime().toString() << "\n";

		}
	} else {
		print("� ������� 'mainfunctionForCreateAndSaveXml' ������ ������ ����, ���������� �������� �� ���� ��������������...");												// ���� ���� � ���������� ������� ������
	}

	fileOut.close();
}