/*
 * Author 
 *		Vagapova Lina
 */

/* !--�������� ����� HTML--! -------------------------------------------------------------------------------------------------------------------------------------------------------------------*/

/* ����������� ��������� */
#include <iostream>
#include <fstream>
#include <list>
#include <windows.h> 
#include <conio.h>
#include <time.h>

/* ����������� ������������ ������ */
#include "../Titem/Titem.h"

/* ������ */
#define print(X) (std::cout << X << "\n")																																// ���������� ����� ������
#define null (std::cout << "\n")																																		// ���������� ������� ������
								
/* ������������ ����������� ���� */
using namespace std;

void precentCreateHTML();																																				// ���������� �����

/* �������� ������� ������ */
/* @param data - ���� ��������� ������� ����� �������� � ����*/
/* @param str - �������� �����*/
void startCreateHTML(list<TItem> data, string str, bool flag) {
	ofstream htmlDoc;																																					// ������� ����

	time_t t;																																							// ��������� �����
	struct tm *t_m;																																						// ������� ���������
	t=time(NULL);																																						// ��
	t_m=localtime(&t);																																					// ���������������

	/* ��������� ������ ���� � ������� ���������� */
	string path(getenv("HOMEPATH"));
	path += "\\Desktop\\LabWorkOne\\";
	path += str;
	path += ".html";

	/* ������� � ��������� ���� ��� ������ */
	htmlDoc.open(path.c_str());

	/* !-������ � ����-! */
	htmlDoc << "<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.0 Transitional//EN\">\n";
	htmlDoc << "<HTML><HEAD>\n" ;
	htmlDoc << "<META http-equiv=\"Content-Type\" content=\"text/html; charset=cp-1261\">\n";
	htmlDoc << "<META name=GENERATOR content=\"MSHTML 8.00.7601.17514\"></HEAD>\n<BODY>\n";
	htmlDoc << "<TITLE>���������</TITLE>\n";
	htmlDoc << "<STYLE type=\"text/css\">\n";
	htmlDoc << "table {border: 2px solid grey; width: 90%; border-collapse: collapse;}\n";
	htmlDoc << "th {border: 2px solid grey;}\n";
	htmlDoc << "td {border: 2px solid grey; padding: 3px; padding-left: 5px;}\n";
	htmlDoc << "</STYLE>\n";
	htmlDoc << "<H5 align=center> <FONT size=10>���������</FONT> </H5>\n";
	htmlDoc << "<H5 align=center> ����: " << t_m->tm_mday  << "." << (t_m->tm_mon + 1) << "." << (t_m->tm_year + 1900) << "</H5>\n";
	htmlDoc << "<H5 align=center> �����: " << t_m->tm_hour << ":" << t_m->tm_min << ":" << t_m->tm_sec << "</H5>\n";
	htmlDoc << "<H5 align=left>�������: �������� �� ������ ���������� ���������� ���������� ���� ��������� � ���������� ������������ ���������������� �� 1 �������?</H5>\n";
	htmlDoc << "<CENTER><TABLE><TBODY>\n";
	htmlDoc << "<TR>\n";
	htmlDoc << "<TH width=10%><SPAN style=\"FONT-SIZE: 14pt\">����� �/�</SPAN></TH\n>" ;
	htmlDoc << "<TH width=10%><SPAN style=\"FONT-SIZE: 14pt\">���������� ����</SPAN></TH\n>" ;
	htmlDoc << "<TH width=15%><SPAN style=\"FONT-SIZE: 14pt\">������� ���������� (���)</SPAN></TH\n>" ;
	htmlDoc << "<TH width=50%><SPAN style=\"FONT-SIZE: 14pt\">����� �������������� ���������� (���)</SPAN></TH\n>" ;
	htmlDoc << "<TH width=15%><SPAN style=\"FONT-SIZE: 14pt\">�����</SPAN></TH\n>" ;
	htmlDoc << "</TR>\n" ;
	
	/* ������ ������ � ���� */
	if (flag) {
		if ( !data.empty() ) {																																					// ���� ��������� ���� �� ������	
			int i = 1;																																							// ����� �/�
			for (auto elem = data.begin(); elem != data.end(); elem++) {																										// ������� ��������� ���������
				{	
					htmlDoc << "<TR>\n<TD width=10%><SPAN style=\"FONT-SIZE: 11pt\"><center>" << i << "</center></SPAN></TD>\n";
					htmlDoc << "<TD width=10%><SPAN style=\"FONT-SIZE: 11pt\"><center>" << elem->getQuantity�ores() << "</center></SPAN></TD>\n";
					htmlDoc << "<TD width=15%><SPAN style=\"FONT-SIZE: 11pt\"><center>" << elem->getClockFrequency().toString() << "</center></SPAN></TD>\n";
					htmlDoc << "<TD width=50%><SPAN style=\"FONT-SIZE: 11pt\"><center>" << elem->getDataSize().toString() << "</center></SPAN></TD>\n";
					if (elem->getTime() == -1) {
						htmlDoc << "<TD width=15%><SPAN style=\"FONT-SIZE: 11pt\"><center> ��� (" << elem->getPrecent().toString() << ")</center></SPAN></TD>\n";	
					} else {
						htmlDoc << "<TD width=15%><SPAN style=\"FONT-SIZE: 11pt\"><center> �� (" << elem->getPrecent().toString() << ")</center></SPAN></TD>\n";	
					}	
				}
				i++;																																							// ����������� ����� �/� �� 1
			}
		} else {
			print("��������� ������ ��� ���������� ��������������� ������ � ������� 'startCreateXML', ���������� �������� ��������������...");
		}
	} else {
		if ( !data.empty() ) {																																					// ���� ��������� ���� �� ������	
			int i = 1;																																							// ����� �/�
			for (auto elem = data.begin(); elem != data.end(); elem++) {																										// ������� ��������� ���������
				{	
					htmlDoc << "<TR>\n<TD width=10%><SPAN style=\"FONT-SIZE: 11pt\"><center>" << i << "</center></SPAN></TD>\n";
					htmlDoc << "<TD width=10%><SPAN style=\"FONT-SIZE: 11pt\"><center>" << elem->getQuantity�ores() << "</center></SPAN></TD>\n";
					htmlDoc << "<TD width=15%><SPAN style=\"FONT-SIZE: 11pt\"><center>" << elem->getClockFrequency().toString() << "</center></SPAN></TD>\n";
					htmlDoc << "<TD width=50%><SPAN style=\"FONT-SIZE: 11pt\"><center>" << elem->getDataSize().toString() << "</center></SPAN></TD>\n";
					if (elem->getTime() == -1) {
						htmlDoc << "<TD width=15%><SPAN style=\"FONT-SIZE: 11pt\"><center> ��� </center></SPAN></TD>\n";	
					} else {
						htmlDoc << "<TD width=15%><SPAN style=\"FONT-SIZE: 11pt\"><center> �� </center></SPAN></TD>\n";	
					}
				}
				i++;																																							// ����������� ����� �/� �� 1
			}
		} else {
			print("��������� ������ ��� ���������� ��������������� ������ � ������� 'startCreateXML', ���������� �������� ��������������...");
		}
	}
	/*-------------------*/

	htmlDoc << "</TBODY></TABLE></CENTER></BODY></HTML>\n";
	/*********************/

	htmlDoc.close();																																						// ��������� ����

	precentCreateHTML();																																					// ����� ���������
	system(path.c_str());
}

/* ����� ������ */
/* ����� ��������� */
void precentCreateHTML() {
	print("�������� ���� ���������� ������ ������ � ��������... (10%)");
	Sleep(500); 
	print("�������� ���� ���������� ������ ������ � ��������... (75%)");
	Sleep(650); 
	print("�������� ���� ���������� ������ ������ � ��������... (100%)");
	Sleep(500); 
	print("���������� ���������... (completed)");

	int precent = 0;
	while (precent <= 100) {
		print(("���� ������� ������, ���������� ��������� (" + to_string(precent) + "%)"));
		Sleep(50); 
		precent++;
	}
	print("������� ������ ����������, ������� �� ��������, ���� �������� � ������� ����������...");
	null;
}