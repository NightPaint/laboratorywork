/*
 * Author 
 *		Vagapova Lina
 */

/* !--�������� ����� XML--! -------------------------------------------------------------------------------------------------------------------------------------------------------------------*/

/* ������������ ���������� */
#include <iostream>  
#include <list>
#include <windows.h> 

/* ������������ ������������ ����� */
#include "../Titem/Titem.h"
#include "../../Libraries/TINY/tinyxml2.h"

/* �������� */
#define print(X) (std::cout << X << "\n")																																// ���������� �����
#define null (std::cout << "\n")																																		// ���������� ������� ������

/* ������������ ����������� ���� */
using namespace tinyxml2;

void precentSeeCreate();																																				// ���������� �����

/* �������� ������� ������ */
/* @param data - ���� TItem ��������� ������� ����� �������� � xml ���� */
void mainfunctionForCreateAndSaveXml(list<TItem> data) {

	tinyxml2::XMLDocument xmlDoc;																																		// Xml ��������
	XMLNode *pRoot = xmlDoc.NewElement("Root");																															// �������� ��� ���������
	xmlDoc.InsertFirstChild(pRoot);																																		// ���������� ��������� ���� � ��������

	

	if ( !data.empty() ) {																																				// ���� ���� �� ������
		for (auto elem = data.begin(); elem != data.end(); elem++) {																									// ���������� ���� ����
			XMLElement *pElement = xmlDoc.NewElement("Elem");																											// �������� ������ ���� 'Elem'
			XMLElement *pQuantity�ores = xmlDoc.NewElement("QuantityCores");																							// �������� ������ ���� 'Quantity �ores'
			XMLElement *pClockFrequency = xmlDoc.NewElement("ClockFrequency");																							// �������� ������ ���� 'Clock Frequency'
			XMLElement *pDataSize = xmlDoc.NewElement("DataSize");																										// �������� ������ ���� 'Data Size'
			XMLElement *pTime = xmlDoc.NewElement("Time");																												// �������� ������ ���� 'Time'

			string quantity�ores = elem->getClockFrequency().toString();
			for(int i=0;i<quantity�ores.length();i++)
				if (quantity�ores[i]=='.')  quantity�ores[i]=',';

			string time = elem->getTime().toString();
			for(int i=0;i<time.length();i++)
				if (time[i]=='.')  time[i]=',';

			pQuantity�ores->SetText(elem->getQuantity�ores());																											// ���������� ���� 'Quantity �ores' �������
			pClockFrequency->SetText(quantity�ores.c_str());																											// ���������� ���� 'Clock Frequency' �������
			pDataSize->SetText(elem->getDataSize().toString().c_str());																									// ���������� ���� 'Data Size' �������
			pTime->SetText(time.c_str());																																// ���������� ���� 'Time' �������

			pElement->InsertFirstChild(pQuantity�ores);																													// ��������� ��� 'Quantity �ores' � ��� 'Element'
			pElement->InsertEndChild(pClockFrequency);																													// ��������� ��� 'Clock Frequency' � ��� 'Element'
			pElement->InsertEndChild(pDataSize);																														// ��������� ��� 'Data Size' � ��� 'Element'
			pElement->InsertEndChild(pTime);																															// ��������� ��� 'Time' � ��� 'Element'

			pRoot->InsertEndChild(pElement);																															// ��������� ��� 'Element' � �������� ���'Root'
		}
	} else {
		print("� ������� 'mainfunctionForCreateAndSaveXml' ������ ������ ����, ���������� �������� �� ���� ��������������...");											// ���� ���� � ���������� ������� ������
	}

	/* �������� ������ ���� � �������� ����� */
	std::string path(getenv("HOMEPATH"));
	path += "\\Desktop\\LabWorkOne\\Data.xml";

	/* ��������� ���� */
	XMLError eResult = xmlDoc.SaveFile(path.c_str());
	if (eResult != XML_SUCCESS) {																																		// �������� �� ���������� ����������
		print("�� ������� ��������� ���� � ������� 'mainfunctionForCreateAndSaveXml'");
		print("���������� ��������� ��� ��������������...");
	}
	
	precentSeeCreate();																																					// ����� ���������
}

/* ����� ������ */
/* ����� ��������� */
void precentSeeCreate() {
	print("�������� ���� ��������� � ���������� ��������������� ������ � ����... (10%)");
	Sleep(500); 
	print("�������� ���� ��������� � ���������� ��������������� ������ � ����... (75%)");
	Sleep(650); 
	print("�������� ���� ��������� � ���������� ��������������� ������ � ����... (100%)");
	Sleep(500); 
	print("�������� ���� ��������� � ���������� ��������������� ������ � ����... (completed)");

	int precent = 0;
	while (precent <= 100) {
		print(("���� ������ ������ � ����, ���������� ��������� (" + to_string(precent) + "%)"));
		Sleep(50); 
		precent++;
	}

	print(("���� ������ ������ � ����, ���������� ��������� (completed)"));
	Sleep(500); 
	print("������ ������ �����������...");
	print("���� ���������� �����... (37%)");
	Sleep(650); 
	print("���� ���������� �����... (61%)");
	Sleep(650); 
	print("���� ���������� �����... (�completed)");
	print("���� �������� � ������� ����������, ������� �� ��������...");

	null;
}