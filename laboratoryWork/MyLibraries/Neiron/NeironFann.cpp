/*
 * Author 
 *		Zagidullin Dmitry
 */

/* !--��������� ����--! -------------------------------------------------------------------------------------------*/

/* ������������ ���������� */
#include <iostream>
#include <stdio.h>
#include <floatfann.h>
#include <fann.h>

/* ������������ �������������� ����� */
#include "NeironFann.h"

/* ������ */
#define print(X) (std::cout << X << "\n")																			// ���������� �����
#define null (std::cout << "\n")																					// ���������� ������� ������

/* ������������ ����������� ���� */
using namespace std;

/* ����������� */
Neiron::Neiron(float desired_errors) { 
	desired_error = (float) desired_errors;																			// ����������� ������
};

void Neiron::startNeiron()
{
	try {
		/* �������� ������ ���� � �������� ����� */
		std::string path(getenv("HOMEPATH"));
		path += "\\Desktop\\LabWorkOne\\Data.txt";

		precentStartNeon();

		/* �������� ��������� ���� � ������ ann */
		ann = fann_create_standard(num_layers, num_input,
			num_neurons_hidden, num_output);

		/* ��������� ������� ��������� ���� */
		fann_set_activation_function_hidden(ann, FANN_SIGMOID_SYMMETRIC);
		fann_set_activation_function_output(ann, FANN_SIGMOID_SYMMETRIC);

		/* �������� */
		fann_train_on_file(ann, path.c_str(), max_epochs,
			epochs_between_reports, desired_error);

	} catch (...) {
		ann = NULL;
	}

	null; /*src*/
}

/* ����� ������ */
/* ����� ��������� */
void Neiron::precentStartNeon() {
	print("���� ��������� � �������� ����... (completed)");
	Sleep(500); 

	int precent = 0;
	while (precent <= 100) {
		print(("�������� ��������� ����, ���������� ��������� (" + to_string(precent) + "%)"));
		Sleep(50); 
		precent++;
	}

	print(("�������� ��������� ����, ���������� ��������� (completed)"));
	print(("��������� ���� ������� ������� �� ��������..."));

	null;
}

/* ������������� */
BigDecimal Neiron::useNeiron(TItem data) {
	fann_type *calc_out;																								// �������� ������
	fann_type input[2];																									// ������� ������

	/* ���������� �������� ������� */
	input[0] = data.getQuantity�ores();
	input[1] = data.getClockFrequency().toInt();

	calc_out = fann_run(ann, input);																					// ������ � ����

	BigDecimal result = (calc_out[0] * 100) / 100;																		// ������� ����������

	return result;																										// ����������� ����������
}

/* �������� �� ������������� */
bool Neiron::isEmpty() {

	if (ann != NULL) 
		return true;																									// ����������� ����������
	else 
		return false;																									// ����������� ����������

}

/* ����������� ��������� ����*/
void Neiron::destroy() {

	/* �������� ������ ���� � �������� ����� */
	std::string pathN(getenv("HOMEPATH"));
	pathN += "\\Desktop\\LabWorkOne\\Data.net";

	/* ���������� ��������� ���� */
	fann_save(ann, pathN.c_str());

	/* ����������� ��������� ���� */
	fann_destroy(ann);
}