#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include <string>

#include "../../Libraries/ALGLIB/dataanalysis.h"
#include "../Titem/Titem.h"
#include "../../Libraries/BigDecimal/BigDecimal.h"

/* ������ -----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------*/
#define print(X) (cout << X << "\n")																																								// ���������� ����� ����� ������
#define null (cout << "\n")																																											// ��������� ������� ������
/*-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------*/

extern void writeData(list<TItem>, bool, int);
void myK_Means();

using namespace alglib;
//using namespace std;

void startALGLIB(list<TItem> datas) {

	print("��������� ������ ������� ������������ �������������:");
	writeData(datas, false, 0);
	null;
	print("��������� ������ ������� ������������ �������������:");

	string inputData = "[";
	for (auto elem = datas.begin(); elem != datas.end(); elem++) {
		double a[] = { elem->getQuantity�ores(), elem->getClockFrequency().toInt(), elem->getQuantity�ores(), elem->getQuantity�ores() };
		inputData += "[" + to_string(elem->getQuantity�ores()) + "," + to_string(elem->getClockFrequency().toInt()) + "," + to_string(elem->getDataSize().toInt()) + "," + to_string(elem->getTime().toInt()) + "],";
	}
	inputData.pop_back();
	inputData += "]";

	real_2d_array xy = inputData.c_str();

	clusterizerstate s;
	ahcreport rep;
	clusterizercreate(s);
	ae_int_t disttype = 2;
	clusterizersetpoints(s, xy, disttype);
	clusterizerrunahc(s, rep);

	for (auto c : rep.z.tostring()) {
		cout << c;
	}
	null;
	null;

	string menuStr = "";
	int menuInt = 0;

	while (true) {
		print("������ ����������� �������������� ������ ������� k-means (���������� �����)");
		print("1. ��");
		print("2. ���");

		std::cin >> menuStr;																																								// ���������� � ����������

		try {
			menuInt = atoi(menuStr.c_str());																																				// �������
		}
		catch (...) {
			print("������ � ������� 'startALGLIB' ��� �������� �����, ���������� �������� �� ���� ��������������!...\n");																	// ������
		}

		if (menuInt == 1) {

			/* ����� */
			myK_Means();
			break;

		}
		else if (menuInt == 2) {
			break;
		}
		else if (menuInt == 0)
			print("�� ����� �� �����! ���������� ��������� �������...\n");																															// ������
		else
			print("�� ����� ������������ ����� ����! ���������� ������� ���������� �����...\n");

	}
}

void myK_Means() {

	null;

	int
		i1 = 0,
		i2 = 0,
		i3 = 0,
		t1 = 0,
		t2 = 0,
		k0[10] = { },
		k1[10] = { },
		k2[10] = { },
		m1 = 0,
		m2 = 0,
		om1 = 0,
		om2 = 0;

	cout << "������� 10 ���� ����� ������ (������, 10 2 3 5 6 7 8 112 458 63258): ";
	for (int i = 0; i < 10; i++) cin >> k0[i];

	cout << "������� ������ ��������� �����: "; cin >> m1;
	cout << "������� ������ ��������� �����: "; cin >> m2;

	null;
	null;

	do {

		om1 = m1;
		om2 = m2;
		i1 = i2 = i3 = 0;

		for (i1 = 0; i1 < 10; i1++) {
			t1 = k0[i1] - m1;
			if (t1 < 0) { t1 = -t1; }

			t2 = k0[i1] - m2;
			if (t2 < 0) { t2 = -t2; }

			if (t1 < t2) {
				k1[i2] = k0[i1];
				i2++;
			} else {
				k2[i3] = k0[i1];
				i3++;
			}
		}

		t2 = 0;
		for (t1 = 0; t1 < i2; t1++) {
			t2 = t2 + k1[t1];
		}
		m1 = t2 / i2;

		t2 = 0;
		for (t1 = 0; t1 < i3; t1++) {
			t2 = t2 + k2[t1];
		}
		m2 = t2 / i3;

		/***/
		cout << "������ ��������� ����� = " << m1 << endl;
		cout << "������� 1: "; for (t1 = 0; t1 < i1; t1++) cout << k1[t1] << " ";
		null;
		cout << "������ ��������� ����� = " << m2 << endl;
		cout << "������� 2: "; for (t2 = 0; t2 < i2; t2++) cout << k2[t2] << " ";

		cout << "\n-------------------------------------------------------------";
		null;
		null;

	} while (m1 != om1 && m2 != om2);
}