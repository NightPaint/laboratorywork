/*
 * Author 
 *		Zagidullin Dmitry
 */

/* !--��������� ����--! ----------------------------------------------------------------*/

#ifndef NEIRON_N_																		// ���� �� ������ ����� ����������� (NEIRON_N_) �� ��� �� ��������� ������ ���������� � ������
#define NEIRON_N_																		// ���������� �� TITEM_H_

/* ������������ ���������� */
#include "../Titem/Titem.h"
#include "../../Libraries/BigDecimal/BigDecimal.h"

class Neiron {

private:
	struct fann *ann;																	// ��������� ����
	static const int num_input = 2;														// ������� �������
    static const int num_output = 1;													// �������� �������
    static const int num_layers = 3;													// ���� ����
    static const int num_neurons_hidden = 3;											// ���������� �������� � ������� ����
    float desired_error;																// ���������� ��� ������
    static const int max_epochs = 150;													// ������������ ���������� ������ �������� ��������� ������� ����� ����
    static const int epochs_between_reports = 10;										// �������� ������������� �������� ����� �������
	void precentStartNeon();															// ���������� �����

public:
	void startNeiron();																	// ��������
	BigDecimal useNeiron(TItem);														// �������������
	Neiron(float desired_errors);														// ����������
	bool isEmpty();																		// �������� �� �������������
	void destroy();																		// ����������� ����

};

#endif NEIRON_N_																		// end